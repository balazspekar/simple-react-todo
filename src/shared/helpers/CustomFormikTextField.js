import {useField} from "formik";
import {TextField} from "@material-ui/core";

export const CustomFormikTextField = ({ placeholder, variant, ...props}) => {
    const [field, meta] = useField(props)
    const errorText = meta.error && meta.touched ? meta.error : ''
    return <TextField
        placeholder={placeholder}
        variant={variant}
        fullWidth
        {...field}
        helperText={errorText}
        error={!!errorText}
    />
}