export default class SpacingHelper {

    static renderSpacing(amount) {
        let result = Array(amount - 1);
        for (let i = 0; i < amount; i++) {
            result[i] = <span className="spacing" key={this.randomIntFromInterval(1, 1000000)}></span>
        }
        return result
    }

    static randomIntFromInterval(min, max) { // min and max included
        return Math.floor(Math.random() * (max - min + 1) + min)
    }


}