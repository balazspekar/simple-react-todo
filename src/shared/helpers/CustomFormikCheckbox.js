import {useField} from "formik";
import {Checkbox, FormControlLabel} from "@material-ui/core";

export const CustomFormikCheckbox = ({label, ...props}) => {
    const [field] = useField(props)
    return <FormControlLabel
        {...field}
        control={<Checkbox />}
        label={label} />
}