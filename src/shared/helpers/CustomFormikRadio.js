import {useField} from "formik";
import {FormControlLabel, Radio} from "@material-ui/core";

export const CustomFormikRadio = ({label, ...props}) => {
    const [field] = useField(props)
    return <FormControlLabel {...field} control={<Radio />} label={label} />
}