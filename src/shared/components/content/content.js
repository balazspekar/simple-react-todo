import "./content.css";
import { Route, Switch } from "react-router-dom";
import HomePage from "../../../pages/homePage/homePage";
import AddPage from "../../../pages/addPage/addPage";
import SimpleAdd from "../../../pages/simpleAddPage/simpleAddPage";

export default function Content() {
  return (
    <div className="content">
      <Switch>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/add" exact>
          <AddPage />
        </Route>
        <Route path="/simple-add" exact>
          <SimpleAdd />
        </Route>
      </Switch>
    </div>
  );
}
