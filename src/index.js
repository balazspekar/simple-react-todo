import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import configureStore from "./redux/configureStore";
import { Provider as ReduxProvider } from "react-redux";
import LocalStorageService from "./services/localStorageService";

/*  It can be useful to pass initial state into the store here if we are using server
 *  rendering or initializing the redux store with data from LocalStorage.
 *
 *  Finding: its a good idea to initialize a default state in the reducers as well.
 *  If it is necessary it can be overwritten here (eg. with data from the LocalStorage)
 *  The 'combineReducer' call is the one that will create a node in the state object (eg notes: [])  */

const store = configureStore(LocalStorageService.restoreState());

ReactDOM.render(
  <React.StrictMode>
    <ReduxProvider store={store}>
      <App />
    </ReduxProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
