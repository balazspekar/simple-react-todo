import "./navbar.css";
import { Link } from "react-router-dom";

const Navbar = () => (
  <div className="navbar">
    <Link to="/">Home</Link>
    <Link to="/add">Add New</Link>
    <Link to="/simple-add">Simple Add New</Link>
  </div>
);

export default Navbar;
