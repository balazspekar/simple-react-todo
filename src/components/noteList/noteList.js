import "./noteList.css";
import { connect } from "react-redux";
import Note from "../note/note";
import { deleteNote } from "../../redux/actions/noteActions";

// this supposed to be a 'Smart Component'
const NoteList = (props) => {
  const handleDeleteNote = (note) => {
    props.dispatch(deleteNote(note));
  };

  return (
    <div className="note-list">
      {props.notes.length === 0 ? <span>There are no notes.</span> : null}
      {props.notes.map((note) => (
        <Note key={note.id} note={note} handleDeleteNote={handleDeleteNote} />
      ))}
    </div>
  );
};

/* When we omit the 2nd param: 'mapDispatchToProps', our component gets a dispatch prop injected automatically
   This func determines what part of the state we expose to our component.
   Be very specific on what to expose from the main store.
   When the store changes -> props change -> React will rerender */
const mapStateToProps = (state) => {
  return {
    notes: state.notes,
  };
};

const connectedStateAndProps = connect(mapStateToProps);
export default connectedStateAndProps(NoteList);
