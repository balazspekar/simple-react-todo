import "./note.css";
import { Whatshot } from "@material-ui/icons";
import { v4 as uuidv4 } from "uuid";

const Note = ({ note, handleDeleteNote }) => {
  const renderCategories = (categories) =>
    categories.map((category) => (
      <span className="category-item" key={uuidv4()}>
        {category}
      </span>
    ));

  const renderImportant = () => (
    <div className="note-content-important">
      <Whatshot fontSize="small" />
      <span>This note is marked as important!</span>
    </div>
  );

  return (
    <div className="note">
      <div className="note-content">
        <span className="note-content-title">{note.title}</span>
        <span className="note-content-description">{note.description}</span>
        <div>{renderCategories(note.categories)}</div>
        {note.isImportant ? renderImportant(note.isImportant) : null}
        <div className="note-owner">written by {note.owner}</div>
        <div className="note-id">{note.id}</div>
        <div>
          <button
            onClick={() => {
              handleDeleteNote(note);
            }}
          >
            delete
          </button>
        </div>
      </div>
    </div>
  );
};

export default Note;
