import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import { Button } from "@material-ui/core";
import { createNote } from "../../redux/actions/noteActions";

const SimpleAddPage = (props) => (
  <>
    <div>Quickly add something to the notes array.</div>
    <br />
    <div>
      <Button
        variant="outlined"
        onClick={() => {
          const uuid = uuidv4();
          const note = {
            id: uuid,
            title: "Predefined Note",
            description: "This is a predefined description.",
            categories: ["work", "home"],
            isImportant: true,
            owner: "McDuck",
          };
          props.dispatch(createNote(note));
        }}
      >
        Add a predefined Note object to the store
      </Button>
    </div>
  </>
);

const mapStateToProps = (state) => {
  return {};
};

const connectedStateAndProps = connect(mapStateToProps);
export default connectedStateAndProps(SimpleAddPage);
