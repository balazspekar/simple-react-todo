import "./addPage.css";
import SpacingHelper from "../../shared/helpers/spacing";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { TextField, Button } from "@material-ui/core";
import { CustomFormikRadio } from "../../shared/helpers/CustomFormikRadio";
import { CustomFormikCheckbox } from "../../shared/helpers/CustomFormikCheckbox";
import { CustomFormikTextField } from "../../shared/helpers/CustomFormikTextField";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { createNote } from "../../redux/actions/noteActions";
import { v4 as uuidv4 } from "uuid";

// Formik Tutorial: https://www.youtube.com/watch?v=FD50LPJ6bjE

const AddPage = (props) => {
  const history = useHistory();

  const navigateToHomePage = () => {
    history.push("/");
  };

  const handleFormikValidation = (values) => {
    const errors = {};
    if (values.title.length < 1) {
      errors.title = "This field is required.";
    }

    if (values.categories.length < 1) {
      errors.categories = "You must select at least one category.";
    }

    if (values.owner === "") {
      errors.owner = "You must select an owner.";
    }

    return errors;
  };

  /* required Formik prop
   *  the data parameter is what the form contains
   *  the second one is an object that can be destructured to get access to extra Formik stuff
   *  */
  const handleFormikOnSubmit = (
    data,
    { setSubmitting, resetForm, isValid }
  ) => {
    setSubmitting(true);
    data.id = uuidv4(); // add an id to the newly created object
    props.dispatch(createNote(data));
    navigateToHomePage();
    setSubmitting(false);
  };

  const formikInitialValues = {
    title: "",
    description: "",
    isImportant: false,
    categories: [],
    owner: "",
  }; // required Formik prop

  return (
    <div className="add">
      <Formik
        initialValues={formikInitialValues}
        onSubmit={handleFormikOnSubmit}
        validate={handleFormikValidation}
      >
        {
          // Formik gives us parameters that we can access as parameters in this function below
          ({
            values,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <Form className="add-form">
              <CustomFormikTextField
                type="input"
                name="title"
                placeholder="This note is about..."
                variant="outlined"
              />
              {SpacingHelper.renderSpacing(1)}
              <Field
                type="input"
                name="description"
                multiline
                variant="outlined"
                rows={4}
                placeholder="Here you can add some extra details..."
                as={TextField}
                fullWidth
              />

              <CustomFormikCheckbox
                type="checkbox"
                name="isImportant"
                label="Mark as important"
              />
              {SpacingHelper.renderSpacing(2)}
              <div>
                <span>
                  <b>How would you categorize this note?</b>
                </span>
                <div>
                  <CustomFormikCheckbox
                    type="checkbox"
                    name="categories"
                    value="household"
                    label="Household"
                  />
                  <CustomFormikCheckbox
                    type="checkbox"
                    name="categories"
                    value="work"
                    label="Work"
                  />
                  <CustomFormikCheckbox
                    type="checkbox"
                    name="categories"
                    value="freelance"
                    label="Freelance"
                  />
                </div>
                <span className="error-message">
                  <ErrorMessage name="categories" />
                </span>
              </div>
              {SpacingHelper.renderSpacing(2)}
              <div>
                <span>
                  <b>Who is going to be owner of this note?</b>
                </span>
                <div>
                  <CustomFormikRadio
                    type="radio"
                    name="owner"
                    value="Uncle Scrooge"
                    label="Uncle Scrooge"
                  />
                  <CustomFormikRadio
                    type="radio"
                    name="owner"
                    value="Della Duck"
                    label="Della Duck"
                  />
                </div>
                <span className="error-message">
                  <ErrorMessage name="owner" />
                </span>
              </div>
              {SpacingHelper.renderSpacing(2)}
              <div className="add-submit-wrapper">
                <Button
                  color="primary"
                  variant="contained"
                  fullWidth
                  type="submit"
                  className="add-submit"
                  disabled={isSubmitting}
                >
                  Save
                </Button>
              </div>
              {SpacingHelper.renderSpacing(3)}
              <hr />
              <div className="debug">
                <pre>
                  <p>Form Debug:</p>
                  {JSON.stringify(values, null, 2)}
                </pre>
                <pre>
                  <p>Errors Debug:</p>
                  {JSON.stringify(errors, null, 2)}
                </pre>
              </div>
            </Form>
          )
        }
      </Formik>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    notes: state.notes,
  };
};

const connectedStateAndProps = connect(mapStateToProps);
export default connectedStateAndProps(AddPage);
