import "./homePage.css";
import NoteList from "../../components/noteList/noteList";

const HomePage = () => (
  <div className="home">
    <NoteList />
  </div>
);

export default HomePage;
