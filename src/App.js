import Header from "./components/header/header";
import './shared/styles/hr.css'
import './shared/styles/container.css'
import './shared/styles/spacing.css'
import Navbar from "./components/navbar/navbar";
import { BrowserRouter as Router } from "react-router-dom";
import Content from "./shared/components/content/content";

function App() {
    return (
        <Router>
            <Header title="Simple React To-Do"/>
            <hr/>
            <div className="container">
                <Navbar />
                <span className="spacing" />
                <Content />
            </div>
        </Router>
    );
}

export default App;
