import * as ActionType from "../actionTypes";

export default function noteReducer(state = [], action) {
  switch (action.type) {
    case ActionType.CREATE_NOTE:
      return [...state, { ...action.note }];
    case ActionType.DELETE_NOTE:
      return state.filter((note) => note.id !== action.note.id);
    default:
      return state;
  }
}
