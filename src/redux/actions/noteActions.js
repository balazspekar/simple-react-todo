import * as ActionType from "../actionTypes";

// This file will hold the Note related 'Action Creators'

export function createNote(note) {
  return { type: ActionType.CREATE_NOTE, note: note }; // this is the action itself
}

export function deleteNote(note) {
  return { type: ActionType.DELETE_NOTE, note: note };
}
