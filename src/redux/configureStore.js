import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers"; // don't have to say index.js because its implied
import reduxImmutableStateInvariant from "redux-immutable-state-invariant";
import LocalStorageService from "../services/localStorageService"; // safety net

export default function configureStore(initialState) {
  // const composeEnhancers =
  //   window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support to Redux Devtools

  const store = createStore(
    rootReducer,
    initialState
    // composeEnhancers(applyMiddleware(reduxImmutableStateInvariant()))
  );

  store.subscribe(() => {
    LocalStorageService.persistState(store.getState());
  });

  return store;
}
