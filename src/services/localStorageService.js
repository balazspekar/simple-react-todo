import { defaultStoreState } from "../redux/defaultStoreState";

export default class LocalStorageService {
  static localStorageKey = "simple-react-todos";

  static persistState(state) {
    localStorage.setItem(this.localStorageKey, JSON.stringify(state));
  }

  static restoreState() {
    const lsContents = localStorage.getItem(this.localStorageKey);
    if (lsContents) {
      return JSON.parse(lsContents);
    } else {
      return defaultStoreState;
    }
  }
}
