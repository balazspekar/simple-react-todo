import {Formik} from 'formik';
import {TextField, Button} from "@material-ui/core";

// Formik Tutorial: https://www.youtube.com/watch?v=FD50LPJ6bjE
export default function SimpleInputForm() {
    const formikInitialValues = {title: ''} // required Formik prop

    /* required Formik prop
    *  the data parameter is what the form contains
    *  the second one is an object that can be destructured to get access to extra Formik stuff
    *  */
    const handleFormikOnSubmit = ((data, {setSubmitting, resetForm}) => {
        setSubmitting(true)
        // make async call
        setTimeout(() => {
            console.log(`Submit:`, data);
            resetForm()
            setSubmitting(false)
        }, 1000)
    })

    return (
        <>
            <Formik initialValues={formikInitialValues} onSubmit={handleFormikOnSubmit}>
                {// Formik gives us parameters that we can access as parameters in this function below
                    ({values, isSubmitting, handleChange, handleBlur, handleSubmit}) => (
                        <form onSubmit={handleSubmit}>
                            <TextField
                                name="title"
                                placeholder="Title Placeholder"
                                value={values.title}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                            <div>
                                <Button disabled={isSubmitting} type="submit">Submit</Button>
                            </div>
                            <pre>
                            {JSON.stringify(values, null, 2)}
                        </pre>
                        </form>
                    )}
            </Formik>
        </>
    )
}